# frozen_string_literal: true

module Lte
  class ApplicationController < ActionController::Base
    rescue_from Error, with: :error_handler

    rescue_from ActiveRecord::RecordNotFound, ActionController::ParameterMissing do |e|
      error_handler(Error.new(400, message: e.message))
    end

    private

    def error_handler(e)
      if e.respond_to?(:return_url) && e.return_url.present?
        redirect_to([e.return_url, { error: e.message }.to_param].join('?'), allow_other_host: true)
        return
      end

      respond_to do |format|
        format.json { render json: e.as_json, status: e.status }
        format.html { render template: 'lte/error', locals: { error: e }, status: e.status }
      end
    end
  end
end

# frozen_string_literal: true

require_dependency 'lte/application_controller'

module Lte
  class RegistrationsController < ApplicationController
    before_action :set_registration, only: %i[show edit update destroy]
    before_action :set_configurations

    # GET /registrations
    def index
      @registrations = registration_scope.all
      respond_to do |format|
        format.json { render json: @registrations }
        format.html
      end
    end

    # GET /registrations/1
    def show
      respond_to do |format|
        format.json { render json: @registration }
        format.html
      end
    end

    # GET /registrations/new
    def new
      @registration = registration_scope.new
      respond_to do |format|
        format.json { render json: @registration, configurations: @configurations }
        format.html
      end
    end

    # GET /registrations/1/edit
    def edit
      respond_to do |format|
        format.json { render json: @registration, configurations: @configurations }
        format.html
      end
    end

    # POST /registrations
    def create
      @registration = registration_scope.new(registration_params)

      if @registration.save
        respond_to do |format|
          format.json { render json: @registration }
          format.html { redirect_to @registration, notice: 'Registration was successfully created.' }
        end
      else
        respond_to do |format|
          format.json { render json: { errors: @registration.errors }, status: :unprocessable_entity }
          format.html { render :new, status: :unprocessable_entity }
        end
      end
    end

    # GET|POST /registrations/dynamic
    def dynamic
      @registration = registration_scope.new
      @registration.register(tool, *params.values_at('openid_configuration', 'registration_token'))
      response.status = :unprocessable_entity unless @registration.persisted?
    end

    # PATCH/PUT /registrations/1
    def update
      if @registration.update(registration_params)
        respond_to do |format|
          format.json { render json: @registration }
          format.html { redirect_to @registration, notice: 'Registration was successfully updated.' }
        end
      else
        respond_to do |format|
          format.json { render json: { errors: @registration.errors }, status: :unprocessable_entity }
          format.html { render :edit, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /registrations/1
    def destroy
      @registration.destroy
      respond_to do |format|
        format.json { head :no_content }
        format.html { redirect_to registrations_url, notice: 'Registration was successfully destroyed.' }
      end
    end

    protected

    def registration_scope
      Registration
    end

    def tool
      Tool.new
    end

    private

    def set_configurations
      @configurations = { base: Tool.new.to_h }
      Tool.config.hooks.each do |name, _|
        hconf = Tool.configuration(name).reject! { |k, v| @configurations[:base][k] == v }
        hconf[LTI_TOOL_KEY].reject! { |k, v| @configurations[:base][LTI_TOOL_KEY][k] == v }
        @configurations[name] = hconf
      end
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_registration
      @registration = registration_scope.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def registration_params
      params.require(:registration).permit(:issuer, :client_id, :deployment_id, :authorization_endpoint, :jwks_uri,
                                           :token_endpoint, :product_family_code, :api_key, :api_secret)
    end

    def dynamic_registration_params
      params.permit(:openid_configuration, :registration_token).tap do |permitted|
        permitted.require(:openid_configuration)
      end
    end
  end
end

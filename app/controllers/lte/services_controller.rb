# frozen_string_literal: true

require_dependency 'lte/application_controller'

module Lte
  class ServicesController < ApplicationController
    include Services::Moodle
    include Services::Blackboard
    include Services::Canvas

    before_action :set_registration

    def lms
      result =
        case @registration.product_family_code
        when 'moodle'
          moodle_get(@registration, lms_params)
        when 'blackboard'
          blackboard_get(@registration, lms_params)
        when 'canvas'
          canvas_get(@registration, lms_params)
        end

      render json: result
    rescue Error => e
      render json: e.message, status: e.status
    end

    protected

    def registration_scope
      Registration
    end

    private

    def set_registration
      @registration = registration_scope.find(session['lte.registration_id'])
    end

    def lms_params
      params.require(:api_request).permit(:method, :host, :path, params: {})
    end
  end
end

# frozen_string_literal: true

require_dependency 'lte/application_controller'

module Lte
  class ToolController < ApplicationController
    use ToolEndpoint, only: %i[login launch]
    before_action :raise_error!

    def login; end

    def launch; end

    def jwks
      render json: {}
    end

    private

    def raise_error!
      raise request.env['lte.error'] if request.env['lte.error']
    end
  end
end

# frozen_string_literal: true

module Lte
  class ApplicationRecord < ActiveRecord::Base
    self.abstract_class = true
  end
end

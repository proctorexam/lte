# frozen_string_literal: true

module Lte
  module BlackboardRegistration
    extend ActiveSupport::Concern
    def issuer=(iss)
      super(iss =~ /blackboard/i ? 'https://blackboard.com' : iss)
    end

    def product_family_code=(code)
      super(code =~ /blackboard/i ? 'blackboard' : code)
    end
  end

  module DynamicRegistration
    extend ActiveSupport::Concern
    include Helpers::Requests

    def register(tool, openid_configuration, registration_token)
      platform_config = get_json(openid_configuration, bearer: registration_token) || {}

      platform = Platform.new(platform_config.merge(openid_configuration:))
      errors.merge!(platform) and return unless platform.valid?

      tool.run_hooks(platform)
      errors.merge!(tool) and return unless tool.valid?

      client = post_json(platform.registration_endpoint, tool.to_json,
                         bearer: registration_token)
      unless client
        errors.add(:registration_endpoint, :invalid)
        return
      end

      assign_attributes(
        issuer: platform_config['issuer'],
        authorization_endpoint: platform_config['authorization_endpoint'],
        jwks_uri: platform_config['jwks_uri'],
        token_endpoint: platform_config['token_endpoint'],
        client_id: client['client_id'],
        deployment_id: client.dig(LTI_TOOL_KEY, 'deployment_id'),

        registration_endpoint: platform_config['registration_endpoint'],
        registration_token:,
        registration_client_uri: client['registration_client_uri'],

        target_link_uri: client.dig(LTI_TOOL_KEY, 'target_link_uri'),

        product_family_code: platform.dig(LTI_PLATFORM_KEY, 'product_family_code')
      )

      save
    end
  end

  class Registration < ApplicationRecord
    belongs_to :provider, polymorphic: true, optional: true

    validates_presence_of :issuer, :client_id, :authorization_endpoint, :jwks_uri, :token_endpoint
    validates_uniqueness_of :client_id, scope: [:issuer], unless: :deployment_id
    validates_uniqueness_of :deployment_id, scope: %i[issuer client_id], if: :deployment_id
    validates_format_of :issuer, :authorization_endpoint, :jwks_uri, :token_endpoint,
                        with: URI::DEFAULT_PARSER.make_regexp

    # Authorization fields, must not change after registration
    %i[issuer client_id deployment_id].each do |ro_attr|
      validates_inclusion_of ro_attr.to_sym, in: ->(rec) { [rec.public_send(:"#{ro_attr}_in_database")] }, on: :update
    end

    store_accessor :config,
                   # tool config
                   :target_link_uri,
                   # from platform-configuration
                   :registration_endpoint,
                   :registration_token,
                   :registration_client_uri,
                   # from lti-platform-configuration
                   :product_family_code,
                   # application specific fields
                   :api_key,
                   :api_secret

    def as_json(opts = {})
      super(opts.merge(methods: %i[target_link_uri product_family_code api_key api_secret]))
    end

    def self.find_for_login(params)
      login_params = params.to_h.symbolize_keys
      iss = login_params[:issuer] || login_params[:iss]
      client_id = login_params[:client_id]
      deployment_id = login_params[:deployment_id] || login_params[:lti_deployment_id]

      return unless iss && client_id && deployment_id

      registrations = order(deployment_id: :desc).where(
        issuer: iss, client_id:, deployment_id: [deployment_id, nil]
      )

      registrations.find { |r| r.deployment_id.present? } || registrations.first
    end

    include BlackboardRegistration
    include DynamicRegistration
  end
end

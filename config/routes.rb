# frozen_string_literal: true

module Lte
  Engine.routes.draw do
    resources :registrations do
      match 'dynamic', on: :collection, via: %i[get post]
    end

    match 'tool/login', via: %i[get post], as: :initiate_login
    match 'tool/launch', via: %i[get post], as: :target_link
    get 'tool/jwks', as: :jwks

    post 'services/lms', as: :lms_service
  end
end

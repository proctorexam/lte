# frozen_string_literal: true

class CreateLteRegistrations < ActiveRecord::Migration[6.1]
  def change
    primary_key_type, foreign_key_type = primary_and_foreign_key_types

    create_table :lte_registrations, id: primary_key_type do |t|
      t.string :issuer, index: true, null: false
      t.string :client_id, index: true, null: false
      t.string :deployment_id, index: true, null: true
      t.string :authorization_endpoint, null: false
      t.string :jwks_uri, null: false
      t.string :token_endpoint, null: false
      t.json :config, default: {}
      t.references :provider, polymorphic: true, null: true, index: false, type: foreign_key_type

      t.timestamps
    end
  end

  private

  def primary_and_foreign_key_types
    config = Rails.configuration.generators
    setting = config.options[config.orm][:primary_key_type]
    primary_key_type = setting || :primary_key
    foreign_key_type = setting || :bigint
    [primary_key_type, foreign_key_type]
  end
end

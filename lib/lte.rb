# frozen_string_literal: true

require 'active_support/dependencies/require_dependency'

require 'lte/version'
require 'lte/engine'
require 'net/http'
require 'lte/validators/items'
require 'lte/validators/same_origin'

module Lte
  LTI_TOOL_KEY = 'https://purl.imsglobal.org/spec/lti-tool-configuration'
  LTI_PLATFORM_KEY = 'https://purl.imsglobal.org/spec/lti-platform-configuration'

  module Helpers
    autoload :Requests, 'lte/helpers/requests'
    autoload :HashAccessor, 'lte/helpers/hash_accessor'
    autoload :InitiatedLoginAuth, 'lte/helpers/initiated_login_auth'
  end

  autoload :Tool, 'lte/models/tool'
  autoload :Platform, 'lte/models/platform'
  autoload :Message, 'lte/models/message'
  autoload :ToolEndpoint, 'lte/endpoints/tool_endpoint'

  autoload :JWKS, 'lte/jwks'
  autoload :Error, 'lte/error'
  autoload :Services, 'lte/services'
end

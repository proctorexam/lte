# frozen_string_literal: true

module Lte
  class ToolEndpoint
    include Helpers::InitiatedLoginAuth
    def initialize(app)
      @app = app
    end

    def call(env)
      @env = env
      return login if request.path =~ /login/
      return launch if request.path =~ /launch/

      @app.call(env)
    rescue Error => e
      clear_launch_session
      env['lte.error'] = e
      @app.call(env)
    end

    def login
      clear_launch_session
      registration = Registration.find_for_login(login_params)
      raise Error, 401 unless registration

      sign_and_redirect registration
    end

    def launch
      state, id_token = launch_params.values_at('state', 'id_token')
      registration_id = session.delete(state)
      raise Error, 401 unless registration_id

      registration = Registration.find_by(id: registration_id)
      raise Error, 401 unless registration

      message = Message.verify!(id_token, registration.jwks_uri, launch_claims(state, registration))
      route = Tool.config.routes.find do |roles, route_to_match|
                route_to_match.matches?(request) && message.role?(roles)
              end&.last
      raise Error, 404 unless route

      session['lte.registration_id'] = registration.id
      env['lte.message'] = message

      request.params.clear
      request.path_parameters = route.defaults
      route.app.serve(request)
    end

    private

    def login_params
      params.slice('iss', 'login_hint', 'target_link_uri', 'client_id', 'deployment_id', 'lti_deployment_id',
                   'lti_message_hint')
    end

    def launch_params
      params.slice('state', 'id_token')
    end

    def sign_and_redirect(registration)
      state = SecureRandom.uuid
      nonce = generate_nonce(state, registration.id)
      session[state] = registration.id
      redirect_to authorization_url(registration, login_params, state, nonce)
    end

    def clear_launch_session
      session.delete('lte.registration_id')
      env.delete('lte.message')
      env.delete('lte.error')
    end

    def request
      @request ||= ActionDispatch::Request.new(env)
    end

    def params
      request.params
    end

    def session
      env['rack.session']
    end

    def redirect_to(url)
      [302, { 'Location' => url }, []]
    end

    attr_reader :env
  end
end

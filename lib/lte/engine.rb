# frozen_string_literal: true

require_dependency 'lte/mapper'
require_dependency 'lte/middlewares/frame_ancestor'

module Lte
  class Engine < ::Rails::Engine
    isolate_namespace Lte

    config.lte = ActiveSupport::OrderedOptions.new

    middleware.use FrameAncestor, /dynamic|login|launch/

    initializer 'lte.frame_ancestor' do |app|
      app.middleware.insert_before Warden::Manager, FrameAncestor, app.config.lte.frame_ancestor_paths
    end

    config.to_prepare do
      overrides = "#{Rails.root}/app/overrides"
      Rails.autoloaders.main.ignore(overrides)
      Dir.glob("#{overrides}/**/*_override.rb").sort.each do |override|
        load override
      end
    end

    ActionDispatch::Routing::Mapper.include Mapper
  end
end

# frozen_string_literal: true

module Lte
  class Error < StandardError
    attr_accessor :status, :errors, :return_url

    def initialize(status, message: nil, errors: nil, return_url: nil)
      super(message || Rack::Utils::HTTP_STATUS_CODES[status])
      @status = status
      @errors = errors
      @return_url = return_url
    end
  end
end

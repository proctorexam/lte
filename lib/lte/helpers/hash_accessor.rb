# frozen_string_literal: true

module Lte
  module Helpers
    module HashAccessor
      extend ActiveSupport::Concern

      class_methods do
        # attr accessor for hashes
        # class Person < Hash
        #   hash_accessor :name, :age
        #   hash_accessor :street, :postcode, to: :address
        # end
        # p = Person.new
        # p.name = 'yolo'
        # p.name == 'yolo'
        # p.postcode = 'EC2'
        # p.postcode == 'EC2'
        def hash_accessor(*names)
          hash = names.last.is_a?(Hash) ? "(self[:'#{names.pop.values.first}'] ||= {})" : 'self'
          define_hash_accessor_methods(hash, *names)
        end

        def define_hash_accessor_methods(hash, *names)
          names.each do |name|
            class_eval <<-METHOD, __FILE__, __LINE__ + 1
              def #{name}
                #{hash}[:#{name}]
              end

              def #{name}=(value)
                #{hash}[:#{name}] = value
              end
            METHOD
          end
        end
      end
    end
  end
end

# frozen_string_literal: true

module Lte
  module Helpers
    module InitiatedLoginAuth
      extend ActiveSupport::Concern

      def generate_nonce(state, id)
        Digest::UUID.uuid_v5(state, id.to_s)
      end

      def authorization_url(registration, params, state, nonce)
        "#{registration.authorization_endpoint}?#{authorization_params(params, state, nonce).to_param}"
      end

      def authorization_params(params, state, nonce)
        params
          .dup
          .compact
          .merge(
            redirect_uri: params.delete('target_link_uri'),
            scope: 'openid', response_type: 'id_token', response_mode: 'form_post', prompt: 'none',
            state:, nonce:
          )
      end

      def launch_claims(state, registration)
        {
          nonce: generate_nonce(state, registration.id),
          iss: registration.issuer,
          aud: registration.client_id,
          target_link_uri: registration.target_link_uri,
          deployment_id: registration.deployment_id
        }
      end
    end
  end
end

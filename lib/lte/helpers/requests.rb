# frozen_string_literal: true

module Lte
  module Helpers
    module Requests
      extend ActiveSupport::Concern

      HDR_JSON = { 'Content-Type' => 'application/json' }.freeze

      module ClassMethods
        def get_json(url, bearer: nil)
          res = Net::HTTP.get_response(URI(url), authorization(HDR_JSON.dup, bearer))
          raise Error.new(res.code, message: res.message, errors: res.body) unless res.is_a?(Net::HTTPSuccess)

          JSON.parse(res.body)
        rescue Net::HTTPError, Errno::ECONNREFUSED, Errno::EADDRNOTAVAIL, ArgumentError => e
          Rails.logger.error ['get_json', e]
          nil
        end

        def post_json(url, body, bearer: nil)
          res = Net::HTTP.post(URI(url), body, authorization(HDR_JSON.dup, bearer))
          raise Error.new(res.code, message: res.message, errors: res.body) unless res.is_a?(Net::HTTPSuccess)

          JSON.parse(res.body)
        rescue Net::HTTPError, Errno::ECONNREFUSED, Errno::EADDRNOTAVAIL, ArgumentError => e
          Rails.logger.error ['post_json', e]
          nil
        end

        def make_uri(host, path, params)
          uri = URI(host)
          uri.path = path
          uri.query = params&.to_query
          uri
        end

        private

        def authorization(headers, bearer)
          headers['Authorization'] = "Bearer #{bearer}" if bearer
          headers
        end
      end

      included do
        extend ClassMethods
        include ClassMethods
      end
    end
  end
end

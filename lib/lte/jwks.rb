# frozen_string_literal: true

require 'jwt'

module Lte
  class JWKS
    RS256 = 'RS256'

    class << self
      def decode(id_token, jwks_uri, alg = RS256)
        JWT.decode(
          id_token,
          nil,
          true,
          algorithms: [alg],
          jwks: proc { JSON.parse(Net::HTTP.get(URI(jwks_uri))) }
        )[0]
      rescue JWT::DecodeError, Net::HTTPError => e
        Rails.logger.error ['[JWKS.decode]', e]
        raise Error, 401
      end

      def encode(payload, alg: RS256, jwk: nil)
        jwk ||= new_jwk(alg)
        token = JWT.encode(payload, jwk.signing_key, jwk[:alg], kid: jwk[:kid])

        [jwk, token]
      end

      def new_jwk(alg = RS256)
        optional_parameters = { use: 'sig', alg: }
        JWT::JWK.new(OpenSSL::PKey::RSA.new(2048), optional_parameters)
      end

      def export(jwk)
        JWT::JWK::Set.new(jwk).export[:keys]
      end
    end
  end
end

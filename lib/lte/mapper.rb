# frozen_string_literal: true

module Lte
  module Mapper
    class ToolMapper
      def initialize(mapper)
        @mapper = mapper
      end

      def message(path, *rest)
        if path.is_a?(Hash)
          roles = path.delete(:roles)
          path[:via] = %i[get post]
        elsif rest.last.is_a?(Hash)
          roles = rest.last.delete(:roles)
          rest.last[:via] = %i[get post]
        else
          rest << { via: %i[get post] }
        end
        @mapper.match(path, *rest)
        last = Rails.application.routes.routes.last
        Tool.config.routes[roles] = last
      end
    end

    extend ActiveSupport::Concern

    def lte(path, opts = {}, &block)
      mount({ Engine => path }.merge(opts))
      ToolMapper.new(self).instance_exec(&block)
    end
  end
end

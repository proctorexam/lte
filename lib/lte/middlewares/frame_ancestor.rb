# frozen_string_literal: true

module Lte
  class FrameAncestor
    POLICY = 'Content-Security-Policy'
    DEST = 'HTTP_SEC_FETCH_DEST'

    def initialize(app, paths)
      @app = app
      @paths = paths
    end

    def call(env)
      _, headers, = response = @app.call(env)

      if @paths.present? && env[DEST]&.ends_with?('frame') && env['PATH_INFO'] =~ @paths
        headers[POLICY] = ";frame-ancestors 'self' https://*"
      end

      response
    end
  end
end

# frozen_string_literal: true

module Lte
  class Message
    CLAIMS = [
      'nonce',
      'iat',
      'exp',
      'iss',
      'aud',
      'sub',
      'given_name',
      'family_name',
      'name',
      'email',
      'https://purl.imsglobal.org/spec/lti/claim/deployment_id',
      'https://purl.imsglobal.org/spec/lti/claim/target_link_uri',
      'https://purl.imsglobal.org/spec/lti/claim/roles',
      'https://purl.imsglobal.org/spec/lti/claim/custom',
      'https://purl.imsglobal.org/spec/lti/claim/context',
      'https://purl.imsglobal.org/spec/lti/claim/message_type',
      'https://purl.imsglobal.org/spec/lti/claim/resource_link',
      'https://purl.imsglobal.org/spec/lti/claim/launch_presentation',
      'https://purl.imsglobal.org/spec/lti/claim/tool_platform'
    ].freeze

    CLAIMS.each do |claim|
      define_method claim.split('/').last do
        self[claim]
      end
    end

    class << self
      def verify!(id_token, jwks_uri, claims)
        decode(id_token, jwks_uri).tap do |msg|
          raise Error.new(401, return_url: msg&.return_url) unless msg&.valid?(claims)
        end
      end

      def decode(id_token, jwks_uri)
        raw = JWKS.decode(id_token, jwks_uri)
        new(raw) if raw
      end
    end

    def encode(jwk)
      _, token = JWKS.encode(@claims, jwk:)
      token
    end

    delegate_missing_to :@claims

    def initialize(claims = {})
      @claims = claims
    end

    def as_json(*)
      @claims.as_json
    end

    def valid?(claims)
      claims[:nonce] == nonce &&
        claims[:iss] == iss &&
        claims[:aud] == aud &&
        claims[:deployment_id] == deployment_id || !claims[:deployment_id] &&
          claims[:target_link_uri] == target_link_uri
    end

    def grep(key)
      @claims.values_at(*@claims.keys.grep(key))
    end

    def first(key)
      grep(key).first
    end

    def role?(reg)
      roles.find { |r| r =~ reg }
    end

    def return_url
      launch_presentation.try(:[], 'return_url')
    end
  end
end

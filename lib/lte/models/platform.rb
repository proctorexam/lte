# frozen_string_literal: true

module Lte
  class Platform < HashWithIndifferentAccess
    include Helpers::HashAccessor
    include ActiveModel::Validations

    DEFAULTS = {
      scopes_supported: ['openid'],
      claims_supported: [],
      LTI_PLATFORM_KEY => {
        messages_supported: [],
        variables: []
      }
    }.freeze

    # open id config
    hash_accessor :issuer,
                  :authorization_server,
                  :authorization_endpoint,
                  :token_endpoint,
                  :userinfo_endpoint,
                  :jwks_uri,
                  :registration_endpoint,
                  :token_endpoint_auth_methods_supported,
                  :token_endpoint_auth_signing_alg_values_supported,
                  :scopes_supported,
                  :response_types_supported,
                  :subject_types_supported,
                  :id_token_signing_alg_values_supported,
                  :claims_supported,
                  :openid_configuration

    hash_accessor :product_family_code, :version, :messages_supported, :variables,
                  to: LTI_PLATFORM_KEY

    # https://www.imsglobal.org/spec/lti-dr/v1p0#platform-configuration
    validates :issuer, presence: true, format: { with: URI::DEFAULT_PARSER.make_regexp }
    validates :authorization_endpoint, format: { with: URI::DEFAULT_PARSER.make_regexp }
    validates :token_endpoint, format: { with: URI::DEFAULT_PARSER.make_regexp }
    validates :jwks_uri, format: { with: URI::DEFAULT_PARSER.make_regexp }
    validates :token_endpoint_auth_methods_supported, items: { any: 'private_key_jwt' }
    validates :token_endpoint_auth_signing_alg_values_supported, items: { any: 'RS256' }
    validates :scopes_supported, items: { any: 'openid' }
    validates :response_types_supported, items: { any: 'id_token' }
    validates :subject_types_supported, items: { any: %w[public pairwise] }
    validates :id_token_signing_alg_values_supported, items: { any: 'RS256' }
    validates :claims_supported, items: { any: String }
    validates :registration_endpoint, same_origin: { with: :issuer }, if: :registration_endpoint
    # https://www.imsglobal.org/spec/lti-dr/v1p0#lti-configuration
    validates :product_family_code, presence: true
    validates :messages_supported, items: { any: { type: String } }
    # https://www.imsglobal.org/spec/lti-dr/v1p0#step-2-discovery-and-openid-configuration
    validates :openid_configuration, same_origin: { with: :issuer }, if: %i[openid_configuration issuer]

    def initialize(attrs = {})
      super(DEFAULTS.deep_merge(attrs))
    end
  end
end

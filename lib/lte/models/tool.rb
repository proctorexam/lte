# frozen_string_literal: true

module Lte
  class Tool < HashWithIndifferentAccess
    module ToolConfig
      def config
        @config ||= begin
          config = ActiveSupport::OrderedOptions.new
          config.tool = ActiveSupport::InheritableOptions.new(DEFAULTS)
          config.hooks = ActiveSupport::InheritableOptions.new
          config.routes = {}
          config
        end
      end

      def configure
        yield config
      end

      def configuration(product_family_code)
        tool = new
        tool.run_hooks(Platform.new(LTI_PLATFORM_KEY => { product_family_code: }))
        tool
      end
    end

    include Helpers::HashAccessor
    include ActiveModel::Validations
    extend ToolConfig

    DEFAULTS = {
      application_type:	'web',
      grant_types: %w[client_credentials implicit],
      response_types:	['id_token'],
      id_token_signed_response_alg: 'RS256',
      token_endpoint_auth_method: 'private_key_jwt',
      scope: 'openid',
      LTI_TOOL_KEY => {}
    }.freeze

    # open id config
    hash_accessor :application_type,
                  :grant_types,
                  :response_types,
                  :id_token_signed_response_alg,
                  :token_endpoint_auth_method,
                  :client_name,
                  :contacts,
                  :logo_uri,
                  :client_uri,
                  :policy_uri,
                  :tos_uri,
                  :redirect_uris,
                  :initiate_login_uri,
                  :jwks_uri,
                  :scope,
                  :client_id,
                  :registration_client_uri

    hash_accessor :domain,
                  :secondary_domains,
                  :target_link_uri,
                  :deployment_id,
                  :custom_parameters,
                  :description,
                  :messages,
                  :claims,
                  to: LTI_TOOL_KEY

    # https://www.imsglobal.org/spec/lti-dr/v1p0#openid-configuration-0
    validates :redirect_uris, presence: true, items: { any: URI::DEFAULT_PARSER.make_regexp }
    validates :response_types, presence: true, items: { any: 'id_token' }
    validates :grant_types, presence: true, items: { any: %w[implict client_credentials] }
    validates :application_type, presence: true, inclusion: { in: ['web'] }
    validates :client_name, presence: true
    validates :jwks_uri, presence: true, format: { with: URI::DEFAULT_PARSER.make_regexp }
    validates :id_token_signed_response_alg, presence: true, inclusion: { in: ['RS256'] }
    validates :token_endpoint_auth_method, presence: true, inclusion: { in: ['private_key_jwt'] }
    validates :initiate_login_uri, presence: true, format: { with: URI::DEFAULT_PARSER.make_regexp }
    validates :scope, presence: true, format: { with: /openid/ }
    # https://www.imsglobal.org/spec/lti-dr/v1p0#lti-configuration-0
    validates :domain, presence: true
    validates :target_link_uri, presence: true, format: { with: URI::DEFAULT_PARSER.make_regexp }
    validates :messages, presence: true, items: { any: { type: String } }
    validates :claims, presence: true, items: { any: String }

    def initialize(attrs = {})
      super(DEFAULTS.deep_merge(attrs))
      Tool.config.tool.each { |key, value| send(:"#{key}=", value) }
    end

    def domain=(host)
      (self[LTI_TOOL_KEY] ||= {})[:domain] = host
      set_urls
    end

    def set_urls
      return unless domain.present?

      opts = { host: domain, protocol: 'https' }
      self.target_link_uri = Engine.routes.url_helpers.target_link_url(opts)
      self.jwks_uri = Engine.routes.url_helpers.jwks_url(opts)
      self.initiate_login_uri = Engine.routes.url_helpers.initiate_login_url(opts)
      self.redirect_uris = [target_link_uri]
    end

    def run_hooks(platform = nil)
      return unless platform

      platform = Platform.new(platform) unless platform.is_a?(Platform)

      hook = Tool.config.hooks.shared
      hook&.call(self, platform)

      hook = platform.product_family_code && Tool.config.hooks[platform.product_family_code]
      hook&.call(self, platform)
    end
  end
end

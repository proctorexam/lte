# frozen_string_literal: true

module Lte
  module Services
    module Moodle
      include Helpers::Requests

      WEBSERVICE_REST_PATH = '/webservice/rest/server.php'

      def moodle_get(registration, params)
        params = { wstoken: registration.api_key, moodlewsrestformat: 'json' }.merge(params['params'] || {})
        get_json(make_uri(registration.issuer, WEBSERVICE_REST_PATH, params))
      end
    end

    module Blackboard
      include Helpers::Requests

      def blackboard_get(registration, params)
        host = params['host']
        token = blackboard_token(host, registration.api_key, registration.api_secret)
        get_json("#{host}#{params['path']}?#{params['params'].to_param}", bearer: token['access_token'])
      end

      private

      def blackboard_token(host, key, secret)
        uri = URI("#{host}learn/api/public/v1/oauth2/token")
        http = Net::HTTP.new(uri.hostname, uri.port)
        http.use_ssl = uri.scheme == 'https'
        req = Net::HTTP::Post.new(uri, 'Accept' => 'application/json')
        req.body = 'grant_type=client_credentials'
        req.basic_auth(key, secret)
        res = http.request(req)
        case res
        when Net::HTTPSuccess
          JSON.parse(res.body)
        else
          raise Error.new(res.code, message: res.message, errors: res.body)
        end
      end
    end

    module Canvas
      include Helpers::Requests

      def canvas_get(registration, params)
        host = params['host']
        token = registration.api_key

        if params['params'].present?
          get_json("#{host}#{params['path']}?#{params['params'].to_param}", bearer: token)
        else
          get_json("#{host}#{params['path']}", bearer: token)
        end
      end
    end
  end
end

# frozen_string_literal: true

module ActiveModel
  module Validations
    class ItemsValidator < ActiveModel::EachValidator
      def validate_each(record, attribute, value)
        return unless value.present?
        return unless options[:any]

        candidates = Array.wrap(options[:any])
        return if candidates.find { |c| value.grep(c) }

        record.errors.add(attribute, "#{value} must contain any of #{options[:any]}", **options)
      end
    end
  end
end

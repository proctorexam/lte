# frozen_string_literal: true

module ActiveModel
  module Validations
    class SameOriginValidator < ActiveModel::EachValidator
      def self.same_host?(src, dst)
        URI(src).host == URI(dst).host
      rescue ArgumentError
        false
      end

      def validate_each(record, attribute, value)
        origin = options[:with] ? record.public_send(options[:with]) : options[:origin]

        return if value.present? && origin.present? && self.class.same_host?(origin, value)

        record.errors.add(attribute, "#{value} has different origin #{origin}", **options.without(:origin))
      end
    end
  end
end

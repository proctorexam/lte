# frozen_string_literal: true

require_relative 'lib/lte/version'

Gem::Specification.new do |spec|
  spec.name        = 'lte'
  spec.version     = Lte::VERSION
  spec.authors     = ['Onur Uyar']
  spec.email       = ['onur@proctorexam.com']
  spec.homepage    = 'https://gitlab.com/proctorexam/lte'
  spec.summary     = 'Summary of Lte.'
  spec.description = 'Description of Lte.'
  spec.license     = 'MIT'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  spec.metadata['allowed_push_host'] = ''

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://gitlab.com/proctorexam/lte'

  spec.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']

  spec.add_dependency 'jwt', '~> 2.7'
  spec.add_dependency 'rails', '>= 6.1.7.6'

  spec.required_ruby_version = '>= 3.1.4'
end

# frozen_string_literal: true

require 'test_helper'

module Lte
  class RegistrationsControllerTest < ActionDispatch::IntegrationTest
    include Engine.routes.url_helpers

    setup do
      @moodle = lte_registrations(:moodle)
    end

    test 'should get index' do
      get registrations_url
      assert_response :success
    end

    test 'should get new' do
      get new_registration_url
      assert_response :success
    end

    test 'should create registration' do
      assert_difference('Registration.count') do
        post registrations_url, params: { registration: {
          authorization_endpoint: 'https://test.dev/authorization_endpoint',
          client_id: 'client_id',
          deployment_id: 'deployment_id',
          issuer: 'https://test.dev',
          jwks_uri: 'https://test.dev/jwks',
          token_endpoint: 'https://test.dev/token'
        } }
      end

      assert_redirected_to registration_url(Registration.last)
    end

    test 'should create dynamic registration' do
      assert_difference('Registration.count') do
        Net::HTTP.stub :get_response, MockResponse.new(file: 'test/support/platform.json') do
          Net::HTTP.stub :post, MockResponse.new(file: 'test/support/client.json') do
            post dynamic_registrations_url, params: { openid_configuration: 'https://server.example.com/connect/config' }
          end
        end
      end

      assert_response :success
    end

    test 'should reject unmatched origins when dynamic registration' do
      assert_difference('Registration.count', 0) do
        Net::HTTP.stub :get_response, MockResponse.new(file: 'test/support/platform.json') do
          Net::HTTP.stub :post, MockResponse.new(file: 'test/support/client.json') do
            post dynamic_registrations_url, params: { openid_configuration: 'https://server.example-hacker.com/connect/config' }
          end
        end
      end

      assert_response :unprocessable_entity
    end

    test 'should show registration' do
      get registration_url(@moodle)
      assert_response :success
    end

    test 'should get edit' do
      get edit_registration_url(@moodle)
      assert_response :success
    end

    test 'should update registration' do
      patch registration_url(@moodle), params: { registration: {
        api_key: @moodle.api_key,
        api_secret: @moodle.api_secret,
        authorization_endpoint: @moodle.authorization_endpoint,
        jwks_uri: @moodle.jwks_uri,
        product_family_code: @moodle.product_family_code,
        token_endpoint: @moodle.token_endpoint
      } }
      assert_redirected_to registration_url(@moodle)
    end

    test 'should destroy registration' do
      assert_difference('Registration.count', -1) do
        delete registration_url(@moodle)
      end

      assert_redirected_to registrations_url
    end
  end
end

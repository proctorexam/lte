# frozen_string_literal: true

require 'test_helper'

module Lte
  class ServicesControllerTest < ActionDispatch::IntegrationTest
    include Engine.routes.url_helpers
    include Helpers::InitiatedLoginAuth

    setup do
      @moodle = lte_registrations(:moodle)
      @moodle_login_params = {
        'iss' => @moodle.issuer,
        'login_hint' => 'login_hint',
        'target_link_uri' => target_link_url(host: 'example.com'),
        'client_id' => @moodle.client_id,
        'lti_deployment_id' => @moodle.deployment_id
      }

      @moodle_message = {
        'https://purl.imsglobal.org/spec/lti/claim/context' => {
          'id' => 'id'
        }
      }

      @blackboard_message = {
        'https://purl.imsglobal.org/spec/lti/claim/tool_platform' => {
          'url' => 'https://blackboard.com'
        },
        'https://purl.imsglobal.org/spec/lti/claim/custom' => {
          'course_pk_string' => 'id'
        }
      }
    end

    # FIX ME: lms service url not found?
    # test "moodle" do
    #   launch_moodle!
    #   course_id = @moodle_message["https://purl.imsglobal.org/spec/lti/claim/context"]['id']

    #   Net::HTTP.stub :get_response, MockResponse.new(body: '{}') do
    #     post lms_service_url, params: {
    #       api_request: {
    #         method: 'GET',
    #         params: {
    #           courseid: course_id,
    #           wsfunction: "core_course_get_contents",
    #         }
    #       }
    #     }
    #   end
    #   assert_response :success
    # end

    # FIX ME: lms service url not found?
    # test "blackboard" do
    #   host = @blackboard_message["https://purl.imsglobal.org/spec/lti/claim/tool_platform"]['url']
    #   course_id = @blackboard_message["https://purl.imsglobal.org/spec/lti/claim/custom"]['course_pk_string']

    #   Net::HTTP.stub :new, MockHTTP.new(MockResponse.new(body: '{}')) do
    #     Net::HTTP.stub :get_response, MockResponse.new(body: '{}') do
    #       post lms_service_url, params: {
    #         api_request: {
    #           method: 'GET',
    #           host: host,
    #           path: `learn/api/public/v1/courses/#{course_id}/contents`,
    #         }
    #       }
    #     end
    #   end

    #   assert_response :success
    # end

    private

    def launch_moodle!
      Digest::UUID.stub :uuid_v5, 'uuid' do
        SecureRandom.stub :uuid, 'uuid' do
          get initiate_login_url(@moodle_login_params)
          assert_redirected_to authorization_url(@moodle, @moodle_login_params, 'uuid', 'uuid')
        end
      end

      claims = {}
      claims['nonce'] = 'uuid'
      claims['iss'] = @moodle.issuer
      claims['aud'] = @moodle.client_id
      claims['https://purl.imsglobal.org/spec/lti/claim/deployment_id'] = @moodle.deployment_id
      claims['https://purl.imsglobal.org/spec/lti/claim/target_link_uri'] = @moodle_login_params['target_link_uri']
      claims['https://purl.imsglobal.org/spec/lti/claim/roles'] = [
        'http://purl.imsglobal.org/vocab/lis/v2/institution/person#Administrator',
        'http://purl.imsglobal.org/vocab/lis/v2/membership#Instructor',
        'http://purl.imsglobal.org/vocab/lis/v2/system/person#Administrator'
      ]
      jwk, id_token = JWKS.encode(claims)
      jwks = JWKS.export(jwk)
      Digest::UUID.stub :uuid_v5, 'uuid' do
        Net::HTTP.stub :get, jwks.to_json do
          post target_link_url('state' => 'uuid', 'id_token' => id_token)
          assert_response :success
        end
      end
    end
  end
end

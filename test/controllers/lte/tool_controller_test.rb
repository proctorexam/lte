# frozen_string_literal: true

require 'test_helper'

module Lte
  class ToolControllerTest < ActionDispatch::IntegrationTest
    include Engine.routes.url_helpers
    include Helpers::InitiatedLoginAuth

    setup do
      @moodle = lte_registrations(:moodle)
      @login_params = {
        'iss' => @moodle.issuer,
        'login_hint' => 'login_hint',
        'target_link_uri' => target_link_url(host: 'example.com'),
        'client_id' => @moodle.client_id,
        'lti_deployment_id' => @moodle.deployment_id
      }
    end

    test 'login bad request' do
      get initiate_login_url
      assert_response :unauthorized
    end

    test 'login unauthorized' do
      get initiate_login_url(iss: 'example.com', client_id: 'yolo', lti_deployment_id: 'asd')
      assert_response :unauthorized
    end

    test 'launch unauthorized' do
      get target_link_url
      assert_response :unauthorized
    end

    test 'launch' do
      Digest::UUID.stub :uuid_v5, 'uuid' do
        SecureRandom.stub :uuid, 'uuid' do
          get initiate_login_url(@login_params)
          assert_redirected_to authorization_url(@moodle, @login_params, 'uuid', 'uuid')
        end
      end

      claims = {}
      claims['nonce'] = 'uuid'
      claims['iss'] = @moodle.issuer
      claims['aud'] = @moodle.client_id
      claims['https://purl.imsglobal.org/spec/lti/claim/deployment_id'] = @moodle.deployment_id
      claims['https://purl.imsglobal.org/spec/lti/claim/target_link_uri'] = @login_params['target_link_uri']
      claims['https://purl.imsglobal.org/spec/lti/claim/roles'] = [
        'http://purl.imsglobal.org/vocab/lis/v2/institution/person#Administrator',
        'http://purl.imsglobal.org/vocab/lis/v2/membership#Instructor',
        'http://purl.imsglobal.org/vocab/lis/v2/system/person#Administrator'
      ]
      jwk, id_token = JWKS.encode(claims)
      jwks = JWKS.export(jwk)
      Digest::UUID.stub :uuid_v5, 'uuid' do
        Net::HTTP.stub :get, jwks.to_json do
          post target_link_url('state' => 'uuid', 'id_token' => id_token)
          assert_response :success
        end
      end
    end

    test 'should get jwks' do
      get jwks_url
      assert_response :success
    end
  end
end

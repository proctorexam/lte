# frozen_string_literal: true

class ApplicationController < ActionController::Base
  # skip_forgery_protection
  def dump
    render json: { message: request.env['lte.message'], params: params }
  end
end

# frozen_string_literal: true

require 'active_support/core_ext/integer/time'

Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded any time
  # it changes. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports.
  config.consider_all_requests_local = true

  # Enable/disable caching. By default caching is disabled.
  # Run rails dev:cache to toggle caching.
  if Rails.root.join('tmp', 'caching-dev.txt').exist?
    config.action_controller.perform_caching = true
    config.action_controller.enable_fragment_cache_logging = true

    config.cache_store = :memory_store
    config.public_file_server.headers = {
      'Cache-Control' => "public, max-age=#{2.days.to_i}"
    }
  else
    config.action_controller.perform_caching = false

    config.cache_store = :null_store
  end

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise exceptions for disallowed deprecations.
  config.active_support.disallowed_deprecation = :raise

  # Tell Active Support which deprecation messages to disallow.
  config.active_support.disallowed_deprecation_warnings = []

  # Raise an error on page load if there are pending migrations.
  config.active_record.migration_error = :page_load

  # Highlight code that triggered database queries in logs.
  config.active_record.verbose_query_logs = true

  # Raises error for missing translations.
  # config.i18n.raise_on_missing_translations = true

  # Annotate rendered view with file names.
  # config.action_view.annotate_rendered_view_with_filenames = true

  # Use an evented file watcher to asynchronously detect changes in source code,
  # routes, locales, etc. This feature depends on the listen gem.
  # config.file_watcher = ActiveSupport::EventedFileUpdateChecker

  # Uncomment if you wish to allow Action Cable access from any origin.
  # config.action_cable.disable_request_forgery_protection = true

  config.hosts << '.ngrok-free.app'

  Lte::Tool.configure do |config|
    config.tool.client_name = 'lte'
    config.tool.domain = '5270-178-226-237-227.ngrok-free.app'

    config.hooks.shared = proc do |tool, platform|
      tool.messages ||= platform.messages_supported
      tool.claims ||= platform.claims_supported
      tool.scope ||= platform.scopes_supported.join(' ') if platform.scopes_supported
    end

    config.hooks.moodle = proc do |tool, platform|
      tool.custom_parameters = platform.variables.index_with { |v| "$#{v}" }
    end

    config.hooks.blackboard = proc do |tool, _platform|
      tool.custom_parameters = {
        'course_id' => '@X@course.id@X@',
        'course_course_name' => '@X@course.course_name@X@',
        'course_pk_string' => '@X@course.pk_string@X@',
        'course_url' => '@X@course.url@X@',
        'course_role' => '@X@course.role@X@',
        'course_locale' => '@X@course.locale@X@',
        'course_ih_nodes' => '@X@course.ih_nodes@X@',
        'course_ih_primary_node' => '@X@course.ih_primary_node@X@',
        'course_ultra_status' => '@X@course.ultra_status@X@',
        'content_id' => '@X@content.id@X@',
        'content_pk_string' => '@X@content.pk_string@X@',
        'content_url' => '@X@content.url@X@',
        'system_site_id' => '@X@system.site_id@X@',
        'course_raw_course_id' => '@X@course.raw_course_id@X@'
      }.freeze
    end
  end
end

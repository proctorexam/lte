# frozen_string_literal: true

require 'active_support/core_ext/integer/time'

# The test environment is used exclusively to run your application's
# test suite. You never need to work with it otherwise. Remember that
# your test database is "scratch space" for the test suite and is wiped
# and recreated between test runs. Don't rely on the data there!

Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  config.cache_classes = true

  # Do not eager load code on boot. This avoids loading your whole application
  # just for the purpose of running a single test. If you are using a tool that
  # preloads Rails for running tests, you may have to set it to true.
  config.eager_load = false

  # Configure public file server for tests with Cache-Control for performance.
  config.public_file_server.enabled = true
  config.public_file_server.headers = {
    'Cache-Control' => "public, max-age=#{1.hour.to_i}"
  }

  # Show full error reports and disable caching.
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false
  config.cache_store = :null_store

  # Raise exceptions instead of rendering exception templates.
  config.action_dispatch.show_exceptions = false

  # Disable request forgery protection in test environment.
  config.action_controller.allow_forgery_protection = false

  # Print deprecation notices to the stderr.
  config.active_support.deprecation = :stderr

  # Raise exceptions for disallowed deprecations.
  config.active_support.disallowed_deprecation = :raise

  # Tell Active Support which deprecation messages to disallow.
  config.active_support.disallowed_deprecation_warnings = []

  # Raises error for missing translations.
  # config.i18n.raise_on_missing_translations = true

  # Annotate rendered view with file names.
  # config.action_view.annotate_rendered_view_with_filenames = true

  Lte::Tool.configure do |config|
    config.tool.client_name = 'tool'
    config.tool.domain = 'example.com'

    config.hooks.shared = proc do |tool, platform|
      tool.messages ||= platform.messages_supported
      tool.claims ||= platform.claims_supported
      tool.scope ||= platform.scopes_supported.join(' ') if platform.scopes_supported
    end

    config.hooks.moodle = proc do |tool, platform|
      tool.custom_parameters = platform.variables.index_with { |v| "$#{v}" }
    end

    config.hooks.blackboard = proc do |tool, _platform|
      tool.custom_parameters = {
        'course_id' => '@X@course.id@X@',
        'course_course_name' => '@X@course.course_name@X@',
        'course_pk_string' => '@X@course.pk_string@X@',
        'course_url' => '@X@course.url@X@',
        'course_role' => '@X@course.role@X@',
        'course_locale' => '@X@course.locale@X@',
        'course_ih_nodes' => '@X@course.ih_nodes@X@',
        'course_ih_primary_node' => '@X@course.ih_primary_node@X@',
        'course_ultra_status' => '@X@course.ultra_status@X@',
        'content_id' => '@X@content.id@X@',
        'content_pk_string' => '@X@content.pk_string@X@',
        'content_url' => '@X@content.url@X@',
        'system_site_id' => '@X@system.site_id@X@',
        'course_raw_course_id' => '@X@course.raw_course_id@X@'
      }
    end
  end
end

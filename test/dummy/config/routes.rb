# frozen_string_literal: true

Rails.application.routes.draw do
  lte '/lte' do
    message 'application/dump', roles: /admin|teacher|instructor/i
    message 'exam/student' => 'application#dump', roles: /student|learner/i
  end
  root to: 'lte/registrations#index'
end

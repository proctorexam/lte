# frozen_string_literal: true

# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20_231_130_120_540) do
  create_table 'lte_registrations', force: :cascade do |t|
    t.string 'issuer', null: false
    t.string 'client_id', null: false
    t.string 'deployment_id'
    t.string 'authorization_endpoint', null: false
    t.string 'jwks_uri', null: false
    t.string 'token_endpoint', null: false
    t.json 'config', default: {}
    t.string 'provider_type'
    t.bigint 'provider_id'
    t.datetime 'created_at', precision: 6, null: false
    t.datetime 'updated_at', precision: 6, null: false
    t.index ['client_id'], name: 'index_lte_registrations_on_client_id'
    t.index ['deployment_id'], name: 'index_lte_registrations_on_deployment_id'
    t.index ['issuer'], name: 'index_lte_registrations_on_issuer'
  end
end

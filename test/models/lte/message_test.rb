# frozen_string_literal: true

require 'test_helper'

module Lte
  class MessageTest < ActiveSupport::TestCase
    setup do
      @message = Message.new(JSON.parse(File.read('test/support/message.json')))
    end

    test 'claims' do
      assert @message.deployment_id == '32'
      assert @message.target_link_uri == 'https://a7dc-178-228-251-69.ngrok-free.app/launch'
      assert @message.roles[0] == 'http://purl.imsglobal.org/vocab/lis/v2/institution/person#Administrator'
      assert @message.custom['context_id'] == '2'
      assert @message.context['id'] == '2'
      assert @message.message_type == 'LtiResourceLinkRequest'
      assert @message.resource_link['title'] == 'test'
      assert @message.launch_presentation['document_target'] == 'iframe'
      assert @message.tool_platform['product_family_code'] == 'moodle'
    end

    test 'return_url' do
      assert @message.return_url == 'https://moodle-development.proctorexam.com/mod/lti/return.php?course=2&launch_container=3&instanceid=12&sesskey=BXpAB11NP9'
    end

    test 'first' do
      assert @message.first(/endpoint/)['lineitems'] == 'https://moodle-development.proctorexam.com/mod/lti/services.php/2/lineitems?type_id=32'
    end
  end
end

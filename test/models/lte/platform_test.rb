# frozen_string_literal: true

require 'test_helper'

module Lte
  class PlatformTest < ActiveSupport::TestCase
    setup do
      @empty = Platform.new
      @decoded = Platform.new(JSON.parse(File.read('test/support/platform.json')))
    end

    test 'initialization' do
      refute @empty.valid?
    end

    test 'valdiations' do
      assert @decoded.valid?
    end

    test 'flattened lti platform configuration attributes' do
      assert @decoded.product_family_code == 'ExampleLMS'
      assert @decoded.messages_supported.length == 2
      assert @decoded.variables.length == 4
    end
  end
end

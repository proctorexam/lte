# frozen_string_literal: true

require 'test_helper'

module Lte
  class RegistrationTest < ActiveSupport::TestCase
    setup do
      @moodle = lte_registrations(:moodle)
      @blackboard = lte_registrations(:blackboard)
    end

    test 'authenticate single tenant deployment' do
      assert Registration.find_for_login(
        iss: 'https://moodle.dev',
        client_id: 'client',
        lti_deployment_id: 'deployment'
      )
    end

    test 'not authenticate single tenant non existing deployment' do
      refute Registration.find_for_login(
        'iss' => 'https://moodle.dev',
        'client_id' => 'client',
        'lti_deployment_id' => SecureRandom.uuid
      )
    end

    test 'authenticate multi tenant client with arbitrary deployment' do
      assert_nil Registration.find_for_login(
        issuer: 'https://blackboard.com',
        client_id: 'client',
        deployment_id: SecureRandom.uuid
      ).deployment_id
    end

    test 'authenticate multi tenant client with existing deployment' do
      assert_equal 'existdeployid', Registration.find_for_login(
        issuer: 'https://blackboard.com',
        client_id: 'client',
        deployment_id: 'existdeployid'
      ).deployment_id
    end

    test 'readonly attributes' do
      @moodle.update(issuer: 'https://test.dev', client_id: 'x', deployment_id: 'x')
      assert @moodle.errors.include?(:issuer)
      assert @moodle.errors.include?(:client_id)
      assert @moodle.errors.include?(:deployment_id)
      assert_equal @moodle.reload.issuer, 'https://moodle.dev'
    end

    test 'validations' do
      errors = Registration.create.errors
      assert errors.include?(:issuer)
      assert errors.include?(:client_id)
      assert errors.include?(:jwks_uri)
      assert errors.include?(:token_endpoint)
      assert errors.include?(:authorization_endpoint)
    end

    test 'uniquness' do
      assert Registration.create(
        issuer: @blackboard.issuer,
        client_id: @blackboard.client_id
      ).errors.of_kind?(
        :client_id, :taken
      )
      assert Registration.create(
        issuer: @moodle.issuer,
        client_id: @moodle.client_id,
        deployment_id: @moodle.deployment_id
      ).errors.of_kind?(
        :deployment_id, :taken
      )
    end

    test 'blackboard constants' do
      registration = Registration.new(issuer: 'https://developer.blackboard.com/',
                                      product_family_code: 'BlackboardLearn')
      assert_equal 'https://blackboard.com', registration.issuer
      assert_equal 'blackboard', registration.product_family_code
    end
  end
end

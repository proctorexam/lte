# frozen_string_literal: true

require 'test_helper'

module Lte
  class ToolTest < ActiveSupport::TestCase
    setup do
      @tool = Tool.new
      @decoded = Tool.new(JSON.parse(File.read('test/support/tool.json')))
    end

    test 'validation' do
      assert @decoded.valid?
    end

    test 'errors' do
      @tool.clear
      refute @tool.valid?
      assert @tool.errors.key?(:client_name)
      assert @tool.errors.key?(:domain)
      assert @tool.errors.key?(:jwks_uri)
      assert @tool.errors.key?(:initiate_login_uri)
      assert @tool.errors.key?(:target_link_uri)
      assert @tool.errors.key?(:messages)
    end

    test 'as json' do
      json = @tool.as_json
      assert_equal 'web', json['application_type']
      assert_equal %w[client_credentials implicit], json['grant_types']
      assert_equal ['id_token'], json['response_types']
      assert_equal 'RS256', json['id_token_signed_response_alg']
      assert_equal 'private_key_jwt', json['token_endpoint_auth_method']
      assert_equal 'example.com', json['https://purl.imsglobal.org/spec/lti-tool-configuration']['domain']
      assert_equal 'https://example.com/lte/tool/launch', json['https://purl.imsglobal.org/spec/lti-tool-configuration']['target_link_uri']
    end

    test 'hooks' do
      @tool.run_hooks(
        LTI_PLATFORM_KEY => {
          'product_family_code' => 'moodle',
          'variables' => ['Context.id']
        }
      )
      json = @tool.as_json
      assert_equal '$Context.id', json['https://purl.imsglobal.org/spec/lti-tool-configuration']['custom_parameters']['Context.id']
    end
  end
end
